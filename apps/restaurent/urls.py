from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r"^$", views.index),
    url(r"^register$", views.register),
    url(r"^login_page$", views.login_page),
    url(r"^login$", views.login),
    url(r"^logout$", views.logout),
    url(r"^dashboard$", views.dashboard),
    url(r"^restaurent_page$", views.restaurent_page),
    url(r"^add_restaurent/create_restaurent$", views.create_restaurent),
    url(r"^restaurent_edit_page/(?P<item_id>\d+)$", views.restaurent_edit_page),
    url(r"^restaurent_edit_page/edit_restaurent/(?P<item_id>\d+)$",
        views.edit_restaurent),
    url(r"^restaurent_detail/(?P<item_id>\d+)$", views.restaurent_detail),
    url(r"^restaurent_review/(?P<item_id>\d+)$", views.restaurent_review),
    url(r"^user_detail_page/(?P<item_id>\d+)$", views.user_detail_page),
    url(r"^restaurent_detail/review_delete/(?P<review_id>\d+)$", views.review_delete),
    url(r"^add_restaurent/(?P<item_id>\d+)$", views.add_restaurent),
    url(r"^remove_restaurent/(?P<item_id>\d+)$", views.remove_restaurent),
    url(r"^delete_restaurent/(?P<item_id>\d+)$", views.delete_restaurent),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
