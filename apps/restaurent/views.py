# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib import messages
from django.db.models import Q
import bcrypt
import re
from time import gmtime, strftime
from datetime import datetime
from . models import *
from django.core.files.storage import FileSystemStorage
# Create your views here.


def index(request):
    return render(request, 'restaurent/index.html')


def register(request):
    if request.method == "POST":
        error = False
        if len(request.POST['name']) < 1 or len(request.POST['user_name']) < 1 or len(
                request.POST['password']) < 1 or len(request.POST['password_confirmation']) < 1:
            messages.error(request, "Fields cannot be blank!")
            error = True

        if not request.POST['name'].isalpha():
            messages.error(
                request, "First name and User name must be non numeric characters")
            error = True
        if not request.POST['user_name'].isalpha():
            messages.error(
                request, "First name and User name must be non numeric characters")
            error = True
        if len(request.POST['password']) < 8:
            messages.error(
                request, "Password must contain at least 8 characters; and at least 1 number, 1 letter, and 1 special character!")
            error = True
        if request.POST['password_confirmation'] != request.POST['password']:
            messages.error(request, "Passwords do not match!")
            error = True
        if error:
            return redirect('/')
        else:
            hashed_pw = bcrypt.hashpw(
                request.POST['password'].encode(), bcrypt.gensalt())
            user = User.objects.create(
                name=request.POST['name'], user_name=request.POST['user_name'], password=hashed_pw)
            return render(request, 'restaurent/login_page')
    return redirect('/')


def login_page(request):
    return render(request, 'restaurent/login_page.html')


def login(request):
    if request.method == "POST":
        user = User.objects.filter(user_name=request.POST["user_name"])
        if len(user) > 0:
            user = user[0]
            if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
                request.session['user_id'] = user.id
                return redirect('/dashboard')
            else:
                messages.error(
                    request, "user_name and/or password is incorrect!")
                return redirect('/')
        else:
            messages.error(
                request, "Register is required, if you already Register user. Please try it again with correct user_name & password")
            return redirect('/')
    else:
        messages.error(request, "user_name is incorrect!")
        return redirect('/')


def logout(request):
    request.session.clear()
    print(request.session.clear())
    return redirect('/')


def dashboard(request):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id'])
    Restaurents = Restaurent.objects.filter(Q(user=user) | Q(members=user))
    Others_Restaurents = Restaurent.objects.exclude(
        Q(user=user) | Q(members=user))
    all_Restaurents = Restaurent.objects.all().order_by('-created_at')
    review = Review.objects.all().order_by('-created_at')
    context = {
        'MY_Restaurent': Restaurents,
        'Other_Restaurent': Others_Restaurents,
        'all_Restureant': all_Restaurents,
    }
    return render(request, 'restaurent/dashboard.html', context)


def restaurent_page(request):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    return render(request, 'restaurent/restaurent_page.html')


def create_restaurent(request):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    if request.method == 'POST':
        print(request.FILES['thumb'])
        myfile = request.FILES['thumb']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
    items = Restaurent.objects.create(
        name=request.POST['name'], foodtype=request.POST['foodtype'], address=request.POST['address'], thumb=filename,
        user=User.objects.get(id=request.session['user_id']))
    return redirect('/dashboard')


def restaurent_detail(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id'])
    Restaurents = Restaurent.objects.get(id=item_id)
    others_Restaurent = Restaurents.members.all()
    review = Review.objects.filter(Restaurents=Restaurents, users=user)
    rating_count = review.count()
    context = {
        'Restaurent_info': Restaurents,
        'others_Restaurents': others_Restaurent,
        'reviews': review,
        'rating_counts': rating_count,
    }
    return render(request, 'restaurent/restaurent_detail.html', context)


def user_detail_page(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    return render(request, 'restaurent/user_detail_page.html')


def restaurent_review(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id'])
    restaurent = Restaurent.objects.get(id=item_id)
    review = Review.objects.create(
        review=request.POST['review'], rating=request.POST['rating'], Restaurents=restaurent, users=user)
    return redirect('/dashboard')


def review_delete(request, review_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id'])
    review = Review.objects.get(id=review_id)
    review.delete()
    return redirect('/restaurent_detail/'+str(review.Restaurents.id))


def add_restaurent(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id']).Restaurents_joined.add(
        Restaurent.objects.get(id=item_id))
    return redirect('/dashboard')


def restaurent_edit_page(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id'])
    Restaurents = Restaurent.objects.get(id=item_id)
    context = {
        'MY_Restaurent': Restaurents,
    }
    return render(request, 'restaurent/edit_restaurent_page.html', context)


def edit_restaurent(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id'])
    restaurent = Restaurent.objects.get(id=item_id)
    restaurent.name = request.POST['name']
    restaurent.foodtype = request.POST['foodtype']
    restaurent.address = request.POST['address']
    restaurent.save()
    return redirect('/dashboard')


def remove_restaurent(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    user = User.objects.get(id=request.session['user_id']).Restaurents_joined.remove(
        Restaurent.objects.get(id=item_id))
    return redirect('/dashboard')


def delete_restaurent(request, item_id):
    if 'user_id' not in request.session:
        return redirect('/login_page')

    restaurent = Restaurent.objects.get(id=item_id)
    user = User.objects.get(id=request.session['user_id'])
    restaurent.delete()
    return redirect('/dashboard')
