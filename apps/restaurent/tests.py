# -*- coding: utf-8 -*-
from django.test import TestCase
from .models import Restaurent
# Create your tests here.


class RestaurentViewTestCase(TestCase):
    def test_string_representation(self):
        entry = Restaurent(name="My entry name")
        self.assertEqual(str(entry), entry.name)
