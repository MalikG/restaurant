# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=100)
    user_name = models.CharField(max_length=100)
    password = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Restaurent(models.Model):
    name = models.CharField(max_length=150)
    foodtype = models.CharField(max_length=150)
    address = models.CharField(max_length=100)
    thumb = models.ImageField(
        upload_to='pictures', max_length=255, blank=True)
    user = models.ForeignKey(User, related_name="Restaurent_creator")
    members = models.ManyToManyField(User, related_name="Restaurents_joined")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Review(models.Model):
    review = models.CharField(max_length=100)
    rating = models.IntegerField(default='0')
    users = models.ForeignKey(User, related_name="user_review")
    Restaurents = models.ForeignKey(
        Restaurent, related_name="Restaurent_review")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
